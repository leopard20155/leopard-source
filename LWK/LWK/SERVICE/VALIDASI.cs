﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LWK.SERVICE
{
    protected class VALIDASI
    {
        protected internal VALIDASI()
        { 
            
        }

        //CEK NUMERIC VALUE
        protected internal bool IsNumeric(string pvalue)
        {
            int i;
            return int.TryParse(pvalue, out i);
        }

        //change keypress to uppercase
        protected internal Char ToHurufBesar(Char pKey)
        {
            return Convert.ToChar(pKey.ToString().ToUpper());
        }

        //Change keypress to numeric only
        protected internal Char NumericOnly(byte pKey)
        {
            Char presult = Convert.ToChar(pKey);

            if (pKey > 57 || pKey < 48)
            {
                if (pKey != 8)
                {
                    if (pKey != 46)
                    {
                        return Convert.ToChar(0);
                    }
                    else { return presult; }
                }
                else { return presult; }
            }
            else { return presult; }
        }
    }
}
