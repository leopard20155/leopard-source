﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace LWK.SERVICE
{
    protected class NORMALIZE
    {
        protected internal NORMALIZE()
        { 
        
        }

        //FORM KOSONG
        protected internal void FormKosong(Form pFrm)
        {
            foreach (Control tb in pFrm.Controls)
            {
                if (tb.GetType() == typeof(GroupBox) || tb.GetType() == typeof(Panel))
                {
                    foreach (Control ctl in tb.Controls)
                    {
                        if (ctl.GetType() == typeof(TextBox))
                        {
                            ctl.Text = string.Empty;
                        }
                        if (ctl.GetType() == typeof(ComboBox))
                        {
                            ctl.Text = string.Empty;
                        }
                    }
                }
                if (tb.GetType() == typeof(TextBox))
                {
                    tb.Text = string.Empty;
                }
                if (tb.GetType() == typeof(ComboBox))
                {
                    tb.Text = string.Empty;
                }
            }
        }
    }
}
