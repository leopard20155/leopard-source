﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LWK.SERVICE
{
    protected class CONVERSION
    {
        protected internal CONVERSION()
        { 
        
        }

        #region Bilangan
        protected internal string GetBilangan(double xjumlah)
        {
            string presult = string.Empty;
            string pjumlah = string.Empty;
            string rupiah = string.Empty;
            string sen = string.Empty;
            string temp = string.Empty;
            string Tmp = string.Empty;
            int i, Desimal, Count;
            string[] place = new string[9];
            place[2] = "ribu ";
            place[3] = "juta ";
            place[4] = "milyar ";
            place[5] = "triliun ";
            Boolean isneg = false;

            //ubah angka jadi string
            xjumlah = Math.Round(xjumlah, 2);
            pjumlah = Convert.ToString(xjumlah).Trim();

            //cek bilangan negatif
            if (pjumlah.Substring(1, 1) == "-")
            {
                pjumlah = pjumlah.Substring(1);
                isneg = true;
            }

            //Posisi desimal, 0 jika bil. bulat
            Desimal = pjumlah.IndexOf(".");

            //Pembulatan sen, dua angka di belakang koma
            //Des = pjumlah.Substring(Desimal + 2);

            if (Desimal > 0)
            {
                Tmp = (pjumlah.Substring(Desimal) + "00").Substring(1);
                if (Tmp.Substring(0, 1) == "0")
                {
                    Tmp = Tmp.Substring(1);
                    sen = GetSatuan(Tmp);
                }
                else
                {
                    sen = GetPuluhan(Tmp);
                }
                pjumlah = (pjumlah.Substring(Desimal - 1)).Trim();
            }

            Count = 1;

            while (pjumlah != "")
            {
                i = pjumlah.Length - 3;
                if (i <= 0) { i = 0; }
                temp = GetRatusan(pjumlah.Substring(i), Count);
                if (temp != "") { rupiah = temp + place[Count] + rupiah; }
                if (pjumlah.Length > 3)
                {
                    pjumlah = pjumlah.Substring(0, pjumlah.Length - 3);
                }
                else { pjumlah = ""; }
                Count += 1;
            }

            if (rupiah == "")
            { rupiah = "nol rupiah"; }
            else { rupiah += "rupiah"; }

            if (isneg)
            {
                return "minus " + rupiah + sen;
            }
            else
            {
                return rupiah + sen;
            }
        }
        protected string GetSatuan(string pdigit)
        {
            switch (Convert.ToInt32(pdigit))
            {
                case 1:
                    return "satu ";
                case 2:
                    return "dua ";
                case 3:
                    return "tiga ";
                case 4:
                    return "empat ";
                case 5:
                    return "lima ";
                case 6:
                    return "enam ";
                case 7:
                    return "tujuh ";
                case 8:
                    return "delapan ";
                case 9:
                    return "sembilan ";
                default:
                    return "";
            }
        }
        protected string GetPuluhan(string sPuluhan)
        {
            int vPuluhan = Convert.ToInt32(sPuluhan);

            if (Convert.ToInt32(sPuluhan.Substring(0, 1)) == 1)
            {
                switch (vPuluhan)
                {
                    case 10:
                        return "sepuluh ";
                    case 11:
                        return "sebelas ";
                    default:
                        return GetSatuan(sPuluhan.Substring(2)) + "belas ";
                }
            }
            else
            {
                return GetSatuan(sPuluhan.Substring(0, 1)) + "puluh " + GetSatuan(sPuluhan.Substring(1));
            }
        }
        protected string GetRatusan(string mynumber, int pcount)
        {
            string stemp = string.Empty;
            string presult = string.Empty;

            if (Convert.ToInt32(mynumber) != 0)
            {
                stemp = "000" + mynumber;
                mynumber = stemp.Substring(stemp.Length - 3);

                //rubah ribuan
                if (mynumber == "001" && pcount == 2)
                {
                    return "se";
                }

                //rubah ratusan
                if (mynumber.Substring(0, 1) != "0")
                {
                    if (mynumber.Substring(0, 1) == "1")
                    {
                        presult = "seratus ";
                    }
                    else
                    {
                        presult = GetSatuan(mynumber.Substring(0, 1)) + "ratus ";
                    }
                }

                //rubah puluhan dan satuan
                if (mynumber.Substring(1, 1) != "0")
                {
                    presult = presult + GetPuluhan(mynumber.Substring(1));
                }
                else
                {
                    presult = presult + GetSatuan(mynumber.Substring(2));
                }
                return presult;
            }
            else { return ""; }
        }
        #endregion

        #region IMAGE
        protected internal byte[] ImageToByte(Image pImg, string pFormat)
        {
            ImageFormat iformat = ImageFormat.Jpeg;
            switch (pFormat.ToUpper())
            {
                case "JPG":
                    iformat = ImageFormat.Jpeg;
                    break;
                case "GIF":
                    iformat = ImageFormat.Gif;
                    break;
                case "PNG":
                    iformat = ImageFormat.Png;
                    break;
            }
            using (var ms = new MemoryStream())
            {
                pImg.Save(ms, iformat);
                return ms.ToArray();
            }
        }
        protected internal Image ByteToImage(byte[] pbyte)
        {
            string strfn = Convert.ToString(DateTime.Now.ToFileTime());
            using (FileStream fs = new FileStream(strfn, FileMode.CreateNew, FileAccess.Write))
            {
                fs.Write(pbyte, 0, pbyte.Length);
                return Image.FromFile(strfn);
            }
        }
        #endregion

        #region XML
        //protected internal 
        #endregion
    }
}
