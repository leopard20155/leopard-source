﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LWK;

namespace LWK.LOGIC
{
    public class CONVERSION
    {
        private SERVICE.CONVERSION Service_Conversion;

        public CONVERSION()
        {
            Service_Conversion = new SERVICE.CONVERSION();
        }

        //Fungsi Bilangan
        public string GetBilangan(double gjumlah)
        {
            return Service_Conversion.GetBilangan(gjumlah);            
        }
       
        //Image To Byte
        public byte[] ImageToByte(Image gImg, string gFormat)
        {
            return Service_Conversion.ImageToByte(gImg, gFormat);
        }
       
        //Byte To Image
        public Image ByteToImage(byte[] gByte)
        {
            return Service_Conversion.ByteToImage(gByte);
        }
                
    }
}
