﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LWK;

namespace LWK.LOGIC
{
    public class VALIDASI
    {
        private SERVICE.VALIDASI Service_Validasi;

        public VALIDASI()
        {
            Service_Validasi = new SERVICE.VALIDASI();
        }

        //CEK NUMERIC VALUE
        public bool IsNumeric(string gvalue)
        {
            return Service_Validasi.IsNumeric(gvalue);
        }
       
        //change keypress to uppercase
        public Char ToHurufBesar(Char gKey)
        {
            return Service_Validasi.ToHurufBesar(gKey);
        }
        
        //Change keypress to numeric only
        public Char NumericOnly(byte gKey)
        {
            return Service_Validasi.NumericOnly(gKey);
        }
        
    }
}
