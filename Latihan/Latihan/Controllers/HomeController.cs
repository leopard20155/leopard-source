﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Latihan.Models;

namespace Latihan.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        GlobalCode gGlobal = new GlobalCode();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ListAnggota()
        {
            List<Anggota> lAnggota=new List<Anggota>();
            lAnggota = gGlobal.GetAllAnggota("Order By Kode_Anggota");
            var data = new GlobalCode.PagedAnggotaModel()
            {
                TotalRows = lAnggota.Count,
                PageSize = 10,
                lAnggota = lAnggota
            };
            return View(data);
        }

        [HttpGet]
        public JsonResult UpdateRecord(string gKode,string gNama,string gOtority,bool gStatus)
        {
            bool result = false;
            try
            {
                Anggota pAnggota = new Anggota();
                pAnggota.Kode_Anggota = gKode;
                pAnggota.Nama_Anggota = gNama;
                pAnggota.Otority = gOtority;
                pAnggota.Status_Aktif = gStatus;
                result = gGlobal.UpdateAnggota(pAnggota);
            }
            catch (Exception)
            {
                result = false;
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SaveRecord(string gKode, string gNama, string gOtority, string gStatus)
        {
            bool result = false;
            try
            {
                Anggota pAnggota = new Anggota();
                pAnggota.Kode_Anggota = gKode;
                pAnggota.Nama_Anggota = gNama;
                pAnggota.Otority = gOtority;
                pAnggota.Status_Aktif = gStatus=="on"? true:false;
                result = gGlobal.InsertAnggota(pAnggota);

            }
            catch (Exception )
            {
                result = false;
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult DeleteRecord(string gKode)
        {
            bool result = false;
            try
            {
                result = gGlobal.DeleteAnggota(gKode);

            }
            catch (Exception)
            {
                result = false;
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }
    }
}
