﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Globalization;
using System.Data.SqlClient;

namespace Latihan.Models
{
    public class GlobalCode
    {        

        private string MyConnection()
        {
            return WebConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        }

        //SELECT SINGLE ANGGOTA
        public Anggota GetAnggota(string gID)
        {
            return pGetAnggota(gID);
        }
        private Anggota pGetAnggota(string pID)
        {
            Anggota pAnggota = new Anggota();
            try
            {
                using (SqlConnection con = new SqlConnection(MyConnection()))
                {
                    con.Open();
                    string str = "SELECT * FROM m_anggota WHERE KODE_ANGGOTA=@1";
                    SqlCommand cmd = new SqlCommand(str, con);
                    cmd.Parameters.Add(new SqlParameter("1", pID));
                    SqlDataReader dr =  cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        pAnggota.Kode_Anggota = dr["Kode_Anggota"].ToString();
                        pAnggota.Nama_Anggota = dr["Nama_Anggota"].ToString();
                        pAnggota.Otority = dr["Otority"].ToString();
                        pAnggota.Status_Aktif = Convert.ToBoolean(dr["Status_Aktif"]);
                        pAnggota.Nama_Status = Convert.ToBoolean(dr["Status_Aktif"])? "Aktif":"Tidak Aktif";
                        pAnggota.Process_Date = Convert.ToDateTime(dr["Process_Date"]);
                    }
                    dr.Close();
                }
                return pAnggota;
            }
            catch (Exception)
            {
                return pAnggota;
                throw;
            }
        }

        //SELECT ALL ANGGOTA
        public List<Anggota> GetAllAnggota(string gParam)
        {
            return pGetAllAnggota(gParam);
        }
        private List<Anggota> pGetAllAnggota(string pParam)
        {
            List<Anggota> pListAnggota = new List<Anggota>();
            try
            {
                using (SqlConnection con = new SqlConnection(MyConnection()))
                {
                    con.Open();
                    string str = "SELECT ROW_NUMBER() OVER(ORDER BY Kode_Anggota ASC)as Nomor,* FROM m_anggota " + pParam;
                    SqlCommand cmd = new SqlCommand(str, con);                   
                    SqlDataReader dr = cmd.ExecuteReader();

                    while (dr.Read())
                    {
                        Anggota pAnggota = new Anggota();
                        pAnggota.Nomor = Convert.ToInt32(dr["Nomor"]);
                        pAnggota.Kode_Anggota = dr["Kode_Anggota"].ToString();
                        pAnggota.Nama_Anggota = dr["Nama_Anggota"].ToString();
                        pAnggota.Otority = dr["Otority"].ToString();
                        pAnggota.Status_Aktif = Convert.ToBoolean(dr["Status_Aktif"]);
                        pAnggota.Nama_Status = Convert.ToBoolean(dr["Status_Aktif"]) ? "Aktif" : "Tidak Aktif";
                        pAnggota.Process_Date = Convert.ToDateTime(dr["Process_Date"]);
                        pListAnggota.Add(pAnggota);
                    }
                    dr.Close();
                }
                return pListAnggota;
            }
            catch (Exception)
            {
                return pListAnggota;
                throw;
            }
        }

        //INSERT ANGGOTA
        public bool InsertAnggota(Anggota gAnggota)
        {
            return pInsertAnggota(gAnggota);
        }
        private bool pInsertAnggota(Anggota pAnggota)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(MyConnection()))
                {
                    con.Open();
                    string str = "INSERT INTO m_anggota(Kode_Anggota,Nama_Anggota,Otority,Status_Aktif,Process_Date)";
                    str += " VALUES(@1,@2,@3,@4,GETDATE())";
                    SqlCommand cmd = new SqlCommand(str, con);
                    cmd.Parameters.Add(new SqlParameter("1", pAnggota.Kode_Anggota));
                    cmd.Parameters.Add(new SqlParameter("2", pAnggota.Nama_Anggota));
                    cmd.Parameters.Add(new SqlParameter("3", pAnggota.Otority));
                    cmd.Parameters.Add(new SqlParameter("4", pAnggota.Status_Aktif));
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        //UPDATE ANGGOTA
        public bool UpdateAnggota(Anggota gAnggota)
        {
            return pUpdateAnggota(gAnggota);
        }
        private bool pUpdateAnggota(Anggota pAnggota)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(MyConnection()))
                {
                    con.Open();
                    string str = "UPDATE m_anggota SET Nama_Anggota=@2,Otority=@3,Status_Aktif=@4,Process_Date=GETDATE()";
                    str += " WHERE Kode_Anggota=@1";
                    SqlCommand cmd = new SqlCommand(str, con);
                    cmd.Parameters.Add(new SqlParameter("1", pAnggota.Kode_Anggota));
                    cmd.Parameters.Add(new SqlParameter("2", pAnggota.Nama_Anggota));
                    cmd.Parameters.Add(new SqlParameter("3", pAnggota.Otority));
                    cmd.Parameters.Add(new SqlParameter("4", pAnggota.Status_Aktif));
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        //DELETE ANGGOTA
        public bool DeleteAnggota(string gID)
        {
            return pDeleteAnggota(gID);
        }
        private bool pDeleteAnggota(string pID)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(MyConnection()))
                {
                    con.Open();
                    string str = "DELETE FROM m_Anggota";
                    str += " WHERE Kode_Anggota=@1";
                    SqlCommand cmd = new SqlCommand(str, con);
                    cmd.Parameters.Add(new SqlParameter("1", pID));
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public class PagedAnggotaModel
        {
            public int TotalRows { get; set; }
            public List<Anggota> lAnggota { get; set; }
            public int PageSize { get; set; }
        }
    }
}