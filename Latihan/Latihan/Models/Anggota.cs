﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Latihan.Models
{
    public class Anggota
    {
        public Anggota()
        {
            Nomor = 0;
            Kode_Anggota = string.Empty;
            Nama_Anggota = string.Empty;
            Otority = string.Empty;
            Status_Aktif = false;
            Nama_Status = string.Empty;
            Process_Date = null;
        }
        public Int32 Nomor { get; set; }
        public string Kode_Anggota { get; set; }
        public string Nama_Anggota { get; set; }
        public string Otority { get; set; }
        public Boolean Status_Aktif { get; set; }
        public string Nama_Status { get; set; }
        public Nullable<DateTime> Process_Date { get;set; }
    }
}