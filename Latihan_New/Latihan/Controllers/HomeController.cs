﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Latihan.Controllers
{
    public class HomeController : Controller
    {
        private LWKEntities db = new LWKEntities();

        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View(db.m_anggota.ToList());
        }

        //
        // GET: /Home/Details/5

        public ActionResult Details(string id = null)
        {
            m_anggota m_anggota = db.m_anggota.Find(id);
            if (m_anggota == null)
            {
                return HttpNotFound();
            }
            return View(m_anggota);
        }

        //
        // GET: /Home/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Home/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(m_anggota m_anggota)
        {
            if (ModelState.IsValid)
            {
                db.m_anggota.Add(m_anggota);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(m_anggota);
        }

        //
        // GET: /Home/Edit/5

        public ActionResult Edit(string id = null)
        {
            m_anggota m_anggota = db.m_anggota.Find(id);
            if (m_anggota == null)
            {
                return HttpNotFound();
            }
            return View(m_anggota);
        }

        //
        // POST: /Home/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(m_anggota m_anggota)
        {
            if (ModelState.IsValid)
            {
                db.Entry(m_anggota).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(m_anggota);
        }

        //
        // GET: /Home/Delete/5

        public ActionResult Delete(string id = null)
        {
            m_anggota m_anggota = db.m_anggota.Find(id);
            if (m_anggota == null)
            {
                return HttpNotFound();
            }
            return View(m_anggota);
        }

        //
        // POST: /Home/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            m_anggota m_anggota = db.m_anggota.Find(id);
            db.m_anggota.Remove(m_anggota);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}