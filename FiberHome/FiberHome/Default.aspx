﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FiberHome.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Home
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--YOU CAN REMARK THIS SLIDE FOR MOMENT--%>
    <div id="slider" > 
            <ul class="rslides" id="slider2">
    	        <li><a href="http://"><img src="../App_Themes/Theme1/SlideHome/01.jpg" alt=""/></a></li>
                <li><a href="http://"><img src="../App_Themes/Theme1/SlideHome/02.jpg" alt=""/></a></li>
                <li><a href="http://"><img src="../App_Themes/Theme1/SlideHome/03.jpg" alt=""/></a></li>  
                <li><a href="http://"><img src="../App_Themes/Theme1/SlideHome/04.jpg" alt=""/></a></li>             
            </ul>
            <script type="text/javascript">
                $(function () {
                    $("#slider2").responsiveSlides({
                        auto: true,
                        pager: false,
                        nav: true,
                        speed: 500,
                        namespace: "callbacks",
                        before: function () {
                            $('.events').append("<li>before event fired.</li>");
                        },
                        after: function () {
                            $('.events').append("<li>after event fired.</li>");
                        }
                    });
                });
	        </script>                                     
        </div>
    <div class="content-center">PLACE YOUR CONTENT HERE!</div>
    
</asp:Content>
